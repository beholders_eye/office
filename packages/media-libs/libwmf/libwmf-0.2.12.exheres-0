# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require github [ user=caolanm tag=v${PV} ]

SUMMARY="Library for converting WMF files"
DESCRIPTION="
libwmf is a library for reading vector images in Microsoft's native Windows
Metafile Format (WMF) and for converting them to more standard/open file formats
such as SVG, EPS, PS, FIG, PNG and JPEG.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-text/ghostscript[>=9.23]
        dev-libs/expat[>=2.0.1]
        media-libs/freetype:2[>=2.0.1]
        media-libs/libpng:=
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.2.8.4-build.patch
    "${FILES}"/${PN}-0.2.8.4-remove_checks.patch
    "${FILES}"/${PN}-0.2.8.4-png15.patch
    "${FILES}"/${PN}-freetype.patch

)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-docdir=/usr/share/doc/${PNVR}
    --with-fontdir=/usr/share/libwmf/fonts
    --with-gsfontdir=/usr/share/fonts/default/ghostscript
    --with-gsfontmap=/usr/share/ghostscript/Resource/Init/Fontmap.GS

    --disable-gd
    --disable-static
    --with-expat
    --with-freetype
    --with-jpeg
    --with-png
    --without-sys-gd
    --with-zlib
    --without-x
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )

src_prepare() {
    default

    autotools_select_versions

    # AFAIU there's no way to pass the "-Ipatches" via autotools.exlib, and it
    # fails without
    edo  autoreconf -vif -Ipatches

    # This obviously won't work with cross-compilation, but *-config scripts
    # seldom do and everybody should just stop using them. Currently at least
    # abiword relies on it though.
    edo sed -e "s:pkg-config:$(exhost --target)-pkg-config:" -i libwmf-config
}

