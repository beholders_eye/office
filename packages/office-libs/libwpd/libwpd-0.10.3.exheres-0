# Copyright 2008, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ]

SUMMARY="Library for importing WordPerfect (tm) documents"
DESCRIPTION="
libwpd is a C++ library designed to help process WordPerfect documents.
It is most commonly used to import WordPerfect documents into other word processors (see below),
but may be useful in other cases as well.
libwpd exposes a simple callback-based API (similar to SAX),
allowing it to be easily plugged into any C or C++ application.
It strictly depends only on an implementation of the STL, although by interfacing with a library
which abstracts the Microsoft OLE format (such as  libgsf),
you gain the ability to import OLE-embedded WordPerfect documents.
"

LICENCES="|| ( LGPL-2.1 MPL-2.0 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/boost   [[ note = [ boost/spirit/include/qi.hpp ] ]]
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen )
    build+run:
        office-libs/librevenge[>=0.0.1]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-fix-build-with-gcc-4.8.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-tools
    --disable-fuzzers
    --disable-werror
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'doc docs' )

